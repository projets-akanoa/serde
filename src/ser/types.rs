use serde::Serialize;
use std::collections::HashMap;
use std::fmt::Debug;

#[derive(Debug, Clone, Serialize)]
pub enum Kind {
    Data(Vec<u8>),
    Struct(StructData),
    UnitStruct(String),
    Map(MapData),
    Seq(SeqData),
    Tuple(TupleData),
    EnumVariantUnit(EnumUnitVariantData),
    EnumVariantTuple(EnumTupleVariantData),
    EnumVariantNewType(EnumNewTypeVariantData),
    EnumVariantStruct(EnumStructVariantData),
    NewTypeStruct(TypeStructData),
    TupleStruct(TupleStructData),
}

#[derive(Debug, Clone, Serialize)]
pub struct StructDataPartial {
    pub(crate) name: String,
    pub(crate) len: usize,
    pub(crate) fields: Vec<String>,
}

#[derive(Debug, Clone, Serialize)]
pub struct StructData {
    pub name: String,
    pub len: usize,
    pub value: HashMap<String, Kind>,
    pub fields: Vec<String>,
}

impl StructData {
    pub fn from_data(name: &str, fields: HashMap<String, Kind>) -> StructData {
        let fields_str = fields.keys().map(|x| x.clone()).collect();

        StructData {
            name: name.to_string(),
            len: fields.len(),
            value: fields,
            fields: fields_str,
        }
    }
}

#[derive(Debug, Clone, Serialize)]
pub struct TupleStructData {
    pub name: String,
    pub value: Vec<Kind>,
    pub len: usize,
}

#[derive(Debug, Clone, Serialize)]
pub struct TypeStructData {
    pub name: String,
    pub value: Box<Kind>,
}

#[derive(Debug, Clone, Serialize)]
pub struct EnumUnitVariantData {
    pub name: String,
    pub variant: String,
    pub variant_index: u32,
}

#[derive(Debug, Clone, Serialize)]
pub struct EnumNewTypeVariantData {
    pub name: String,
    pub variant: String,
    pub variant_index: u32,
    pub value: Box<Kind>,
}

#[derive(Debug, Clone, Serialize)]
pub struct EnumStructVariantData {
    pub name: String,
    pub variant: String,
    pub variant_index: u32,
    pub value: StructData,
}

#[derive(Debug, Clone, Serialize)]
pub struct EnumTupleVariantData {
    pub value: Vec<Kind>,
    pub name: String,
    pub variant: String,
    pub variant_index: u32,
    pub len: usize,
}

#[derive(Debug, Clone, Serialize)]
pub struct TupleData {
    pub value: Vec<Kind>,
    pub len: usize,
}

#[derive(Debug, Clone, Serialize)]
pub struct MapData {
    pub value: Vec<(Kind, Kind)>,
    pub len: Option<usize>,
}

#[derive(Debug, Clone, Serialize)]
pub struct SeqData {
    pub value: Vec<Kind>,
    pub len: Option<usize>,
}
