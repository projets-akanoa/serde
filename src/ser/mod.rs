mod serializer;
pub mod types;

use crate::errors::Result;
use crate::KeysValues;
use foundationdb::tuple::Subspace;
use serde::Serialize;
use serializer::Serializer;

pub fn to_keys_values<T>(value: &T, subspace: &Subspace) -> Result<KeysValues>
where
    T: Serialize,
{
    let mut serializer = Serializer::new(subspace);
    value.serialize(&mut serializer)?;
    Ok(serializer.get_keys_values())
}
