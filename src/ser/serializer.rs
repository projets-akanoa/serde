use crate::errors::Error;
use crate::ser::types::{
    EnumNewTypeVariantData, EnumStructVariantData, EnumTupleVariantData, EnumUnitVariantData, Kind,
    MapData, SeqData, StructData, StructDataPartial, TupleData, TupleStructData, TypeStructData,
};
use crate::{KeyValue, KeysValues};
use foundationdb::tuple::{pack, Subspace};
use serde::Serialize;
use std::collections::HashMap;
use std::ops::Deref;

fn handle_value(subspace: Subspace, value: &Kind, out: &mut Vec<KeyValue>) {
    match value {
        Kind::Data(value) => {
            let subspace = subspace.subspace(&("_data"));
            let key = subspace.into_bytes();
            out.push((key, value.clone()));
        }
        Kind::Seq(data) => {
            let subspace = subspace.subspace(&("_seq"));
            let key = subspace.pack(&("_len"));
            out.push((key, pack(&data.len)));

            for (index, element) in data.value.iter().enumerate() {
                let subspace = subspace.subspace(&(index));
                handle_value(subspace, element, out);
            }
        }
        Kind::Tuple(data) => {
            let subspace = subspace.subspace(&("_tuple"));

            let key = subspace.pack(&("_len"));
            out.push((key, pack(&data.len)));

            for (index, element) in data.value.iter().enumerate() {
                let subspace = subspace.subspace(&(index));
                handle_value(subspace, element, out);
            }
        }
        Kind::Map(data) => {
            let subspace = subspace.subspace(&("_map"));
            let key = subspace.pack(&("_len"));
            out.push((key, pack(&data.len)));
            for (entry_id, (key, element)) in data.value.iter().enumerate() {
                let subspace_key = subspace.subspace(&("_key", entry_id));
                handle_value(subspace_key, key, out);
                let subspace_value = subspace.subspace(&("_value", entry_id));
                handle_value(subspace_value, element, out);
            }
        }
        Kind::Struct(data) => {
            let subspace = subspace.subspace(&("_struct", &data.name));

            for (key, element) in &data.value {
                let subspace = subspace.subspace(&("_field", key));
                handle_value(subspace, element, out);
            }
        }
        Kind::UnitStruct(struct_name) => {
            let key = subspace.pack(&("_unit_struct", struct_name));
            out.push((key, vec![]))
        }
        Kind::EnumVariantUnit(value) => {
            let key = subspace.pack(&("_enum", &value.name, &value.variant));
            out.push((key, vec![]))
        }
        Kind::EnumVariantTuple(data) => {
            let subspace = subspace.subspace(&("_enum", &data.name, &data.variant, "_tuple"));

            let key = subspace.pack(&("_len"));
            out.push((key, pack(&data.len)));

            for (index, element) in data.value.iter().enumerate() {
                let subspace = subspace.subspace(&(index));
                handle_value(subspace, element, out);
            }
        }
        Kind::EnumVariantNewType(data) => {
            let subspace = subspace.subspace(&("_enum", &data.name, &data.variant));
            let value = data.value.deref();
            handle_value(subspace, value, out);
        }
        Kind::EnumVariantStruct(data) => {
            let subspace = subspace.subspace(&("_enum", &data.name, &data.variant));
            let value = Kind::Struct(StructData::from_data(
                &data.variant,
                data.value.value.clone(),
            ));
            handle_value(subspace, &value, out);
        }
        Kind::NewTypeStruct(data) => {
            let subspace = subspace.subspace(&("_struct", &data.name));
            let value = data.value.deref();
            handle_value(subspace, value, out);
        }
        Kind::TupleStruct(data) => {
            let subspace = subspace.subspace(&("_struct", &data.name, "_tuple"));
            let key = subspace.pack(&("_len"));
            out.push((key, pack(&data.len)));
            for (index, element) in data.value.iter().enumerate() {
                let subspace = subspace.subspace(&(index));
                handle_value(subspace, element, out);
            }
        }
    }
}

pub struct Serializer {
    subspace: Subspace,
    accumulator: HashMap<String, Kind>,
    scalar_accumulator: Vec<Kind>,
    tuple_data_progress: Option<TupleData>,
    enum_tuple_data_progress: Option<EnumTupleVariantData>,
    seq_data_progress: Option<SeqData>,
    tuple_struct_progress: Option<TupleStructData>,
    map_data_progress: Option<MapData>,
    struct_data_progress: Option<StructDataPartial>,
    enum_struct_progress: Option<EnumStructVariantData>,
}

impl Serializer {
    pub fn new(subspace: &Subspace) -> Self {
        Serializer {
            accumulator: HashMap::new(),
            scalar_accumulator: vec![],
            subspace: subspace.clone(),
            tuple_data_progress: None,
            enum_tuple_data_progress: None,
            seq_data_progress: None,
            tuple_struct_progress: None,
            map_data_progress: None,
            struct_data_progress: None,
            enum_struct_progress: None,
        }
    }

    pub fn get_keys_values(&self) -> KeysValues {
        let mut out = vec![];

        let mut subspace = self.subspace.clone();

        if let Some(struct_data) = &self.struct_data_progress {
            subspace = self.subspace.subspace(&("_struct", &struct_data.name));

            for (key, value) in &self.accumulator {
                let subspace = subspace.subspace(&("_field", key));
                handle_value(subspace, &value, &mut out);
            }
        } else {
            for value in &self.scalar_accumulator {
                handle_value(subspace.clone(), &value, &mut out);
            }
        }

        KeysValues { inner: out }
    }

    fn serialize_data<T>(&self, value: &T) -> Result<Kind, self::Error>
    where
        T: Serialize,
    {
        let mut serializer = Serializer::new(&self.subspace);
        let value = value.serialize(&mut serializer)?;
        Ok(value)
    }
}

impl<'a> serde::ser::Serializer for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;
    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, v: bool) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_i8(self, v: i8) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&(v as i32)));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_i16(self, v: i16) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_i32(self, v: i32) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_i64(self, v: i64) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_u8(self, v: u8) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&(v as u32)));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_u16(self, v: u16) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_u32(self, v: u32) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_u64(self, v: u64) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_f32(self, v: f32) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_f64(self, v: f64) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_char(self, v: char) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v.to_string()));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_str(self, v: &str) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&v));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&()));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_some<T: ?Sized>(self, value: &T) -> Result<Self::Ok, Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        self.scalar_accumulator.push(value.clone());
        Ok(value)
    }

    fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
        let data = Kind::Data(pack(&()));
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_unit_struct(self, name: &'static str) -> Result<Self::Ok, Self::Error> {
        let data = Kind::UnitStruct(name.to_string());
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_unit_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
    ) -> Result<Self::Ok, Self::Error> {
        let data = Kind::EnumVariantUnit(EnumUnitVariantData {
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index,
        });
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_newtype_struct<T: ?Sized>(
        self,
        name: &'static str,
        value: &T,
    ) -> Result<Self::Ok, Self::Error>
    where
        T: Serialize,
    {
        let mut serializer = Serializer::new(&self.subspace);
        let value = value.serialize(&mut serializer)?;
        let data = Kind::NewTypeStruct(TypeStructData {
            name: name.to_string(),
            value: Box::new(value),
        });
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_newtype_variant<T: ?Sized>(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        value: &T,
    ) -> Result<Self::Ok, Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        let data = EnumNewTypeVariantData {
            value: Box::new(value),
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index,
        };
        let data = Kind::EnumVariantNewType(data);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq, Self::Error> {
        self.seq_data_progress = Some(SeqData { value: vec![], len });
        Ok(self)
    }

    fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Self::Error> {
        self.tuple_data_progress = Some(TupleData { value: vec![], len });
        Ok(self)
    }

    fn serialize_tuple_struct(
        self,
        name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleStruct, Self::Error> {
        self.tuple_struct_progress = Some(TupleStructData {
            name: name.to_string(),
            value: vec![],
            len,
        });
        Ok(self)
    }

    fn serialize_tuple_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleVariant, Self::Error> {
        self.enum_tuple_data_progress = Some(EnumTupleVariantData {
            value: vec![],
            name: name.to_string(),
            variant: variant.to_string(),
            len,
            variant_index,
        });
        Ok(self)
    }

    fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
        self.map_data_progress = Some(MapData { value: vec![], len });
        Ok(self)
    }

    fn serialize_struct(
        self,
        name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStruct, Self::Error> {
        self.struct_data_progress = Some(StructDataPartial {
            name: name.to_string(),
            len,
            fields: vec![],
        });
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        name: &'static str,
        variant_index: u32,
        variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStructVariant, Self::Error> {
        let struct_data = StructData {
            name: name.to_string(),
            value: Default::default(),
            fields: vec![],
            len,
        };
        self.enum_struct_progress = Some(EnumStructVariantData {
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index,
            value: struct_data,
        });
        Ok(self)
    }
}

impl<'a> serde::ser::SerializeSeq for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        self.seq_data_progress.as_mut().unwrap().value.push(value);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let value = self.seq_data_progress.clone().unwrap();
        self.seq_data_progress = None;
        let data = Kind::Seq(value);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }
}

impl<'a> serde::ser::SerializeTuple for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        self.tuple_data_progress.as_mut().unwrap().value.push(value);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let value = self.tuple_data_progress.clone().unwrap();
        self.tuple_data_progress = None;
        let data = Kind::Tuple(value);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }
}

impl<'a> serde::ser::SerializeTupleVariant for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        self.enum_tuple_data_progress
            .as_mut()
            .unwrap()
            .value
            .push(value);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let value = self.enum_tuple_data_progress.clone().unwrap();
        self.tuple_data_progress = None;
        let data = Kind::EnumVariantTuple(value);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }
}

impl<'a> serde::ser::SerializeTupleStruct for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        self.tuple_struct_progress
            .as_mut()
            .unwrap()
            .value
            .push(value);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let value = self.tuple_struct_progress.clone().unwrap();
        self.tuple_struct_progress = None;
        let data = Kind::TupleStruct(value);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }
}

impl<'a> serde::ser::SerializeStruct for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_field<T: ?Sized>(
        &mut self,
        key: &'static str,
        value: &T,
    ) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        let mut serializer = Serializer::new(&self.subspace);
        let value = value.serialize(&mut serializer)?;
        self.accumulator.insert(key.to_string(), value);
        self.struct_data_progress
            .as_mut()
            .unwrap()
            .fields
            .push(key.to_string());
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let partial_data = self.struct_data_progress.as_ref().unwrap().clone();
        Ok(Kind::Struct(StructData {
            name: partial_data.name.clone(),
            len: partial_data.len,
            value: self.accumulator.clone(),
            fields: partial_data.fields,
        }))
    }
}

impl<'a> serde::ser::SerializeStructVariant for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_field<T: ?Sized>(
        &mut self,
        key: &'static str,
        value: &T,
    ) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        let value = self.serialize_data(&value)?;
        let acc = &mut self.enum_struct_progress.as_mut().unwrap().value;
        acc.value.insert(key.to_string(), value);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let struct_data = self.enum_struct_progress.as_ref().unwrap().clone();
        self.enum_struct_progress = None;
        let data = Kind::EnumVariantStruct(struct_data);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }
}

impl<'a> serde::ser::SerializeMap for &'a mut Serializer {
    type Ok = Kind;
    type Error = Error;

    fn serialize_key<T: ?Sized>(&mut self, _key: &T) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        unreachable!()
    }

    fn serialize_value<T: ?Sized>(&mut self, _value: &T) -> Result<(), Self::Error>
    where
        T: Serialize,
    {
        unreachable!()
    }

    fn serialize_entry<K: ?Sized, V: ?Sized>(
        &mut self,
        key: &K,
        value: &V,
    ) -> Result<(), Self::Error>
    where
        K: Serialize,
        V: Serialize,
    {
        let key = self.serialize_data(&key)?;
        let value = self.serialize_data(&value)?;
        let data = (key, value);
        self.map_data_progress.as_mut().unwrap().value.push(data);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        let map_data = self.map_data_progress.as_ref().unwrap().clone();
        self.map_data_progress = None;
        let data = Kind::Map(map_data);
        self.scalar_accumulator.push(data.clone());
        Ok(data)
    }
}
