use std::collections::HashSet;
use std::fmt::{Display, Formatter};

pub mod errors;
pub mod ser;
pub mod de;

#[derive(Default, Debug)]
pub struct KeysValues {
    pub inner: Vec<KeyValue>,
}

pub type KeyValue = (Vec<u8>, Vec<u8>);

impl PartialEq for KeysValues {
    fn eq(&self, other: &Self) -> bool {
        let set = self
            .inner
            .iter()
            .fold(HashSet::<KeyValue>::new(), |mut acc, e| {
                acc.insert(e.clone());
                acc
            });

        for element in &other.inner {
            if let None = set.get(element) {
                return false;
            }
        }

        true
    }
}

impl Display for KeysValues {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut out = format!("{{\n");
        self.inner.iter().for_each(|(key, value)| {
            let key_string = String::from_utf8_lossy(key);
            out = format!("{}    {:?} => {:?}\n", out, key_string, value);
        });
        out = format!("{}{}", out, "}");

        write!(f, "{}", out)
    }
}
