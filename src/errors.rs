use foundationdb::tuple::{unpack, Element, PackError, Subspace};
use serde::ser::StdError;
use std::fmt;
use std::fmt::{Debug, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

pub enum Error {
    // Placeholder variant
    Message(String),
    PackError(PackError),
    SubspaceNotFound { subspace: Subspace },
    EmptyElementSet,
    SeqDataNotInitialized,
    NotABooleanValue { subspace: Subspace },
    NotAnIntegerValue { subspace: Subspace },
    NotAFloatValue { subspace: Subspace },
    NotADoubleValue { subspace: Subspace },
    NotStringValue { subspace: Subspace },
    CharDeserializationNotSupported { subspace: Subspace },
}

impl StdError for Error {}

impl Debug for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "test")
    }
}

impl serde::ser::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        todo!()
    }
}

impl serde::de::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::Message(msg.to_string())
    }
}

impl Display for Error {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        match self {
            Error::Message(msg) => formatter.write_str(msg),
            Error::NotAnIntegerValue { subspace } => formatter.write_str("not an integer value"),
            Error::PackError(err) => formatter.write_str(&*err.to_string()),
            Error::SubspaceNotFound { subspace } => {
                let key = String::from_utf8_lossy(subspace.bytes());
                formatter.write_str(&*format!("subspace not found: {:?}", key))
            }
            Error::EmptyElementSet => formatter.write_str("empty element set"),
            Error::NotABooleanValue { .. } => formatter.write_str("not a boolean value"),
            Error::NotAFloatValue { .. } => formatter.write_str("not a float value"),
            Error::NotADoubleValue { .. } => formatter.write_str("not a double value"),
            Error::NotStringValue { .. } => formatter.write_str("not a string value"),
            Error::CharDeserializationNotSupported { .. } => {
                formatter.write_str("deserialization not supported")
            }
            _ => unreachable!(),
        }
    }
}

impl From<PackError> for Error {
    fn from(err: PackError) -> Self {
        Error::PackError(err)
    }
}
