pub mod deserializer;
mod types;

use crate::de::deserializer::Deserializer;
use crate::errors::Result;
use crate::KeysValues;
use foundationdb::tuple::Subspace;
use serde::Deserialize;

pub fn from_keys_values<'de, T>(kvs: &'de KeysValues, subspace: Option<&Subspace>) -> Result<T>
where
    T: Deserialize<'de>,
{
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);

    let mut deserializer = Deserializer::try_new(kvs, subspace)?;
    let t = T::deserialize(&mut deserializer)?;
    Ok(t)
}
