use crate::de::types::SeqData;
use crate::errors::{Error, Result};
use crate::KeysValues;
use foundationdb::tuple::{unpack, Element, Subspace};
use serde::de::{DeserializeSeed, MapAccess, SeqAccess, Visitor};
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::hash::{Hash, Hasher};

struct Wrapper(Subspace);

impl PartialEq for Wrapper {
    fn eq(&self, other: &Self) -> bool {
        self.0.bytes() == other.0.bytes()
    }
}

impl Eq for Wrapper {}

impl Hash for Wrapper {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.bytes().hash(state);
        state.finish();
    }
}

impl Debug for Wrapper {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", String::from_utf8_lossy(self.0.bytes()))
    }
}

struct StructDataPartial {
    name: String,
    fields: Vec<String>,
    field: String,
}

pub struct Deserializer<'de> {
    kvs: HashMap<Wrapper, Vec<Element<'de>>>,
    subspace: Subspace,
    old_subspace: Subspace,
    struct_progress: Option<StructDataPartial>,
    seq_progress: Option<SeqData>,
}

impl<'de> Deserializer<'de> {
    pub fn try_new(kvs: &'de KeysValues, subspace: &Subspace) -> Result<Self> {
        let mut map: HashMap<Wrapper, Vec<Element>> = HashMap::new();

        for (key, value) in &kvs.inner {
            let subspace = Subspace::from_bytes(key.clone());
            let element: Vec<Element> = unpack(value)?;
            map.insert(Wrapper(subspace), element);
        }

        Ok(Deserializer {
            kvs: map,
            subspace: subspace.clone(),
            old_subspace: subspace.clone(),
            struct_progress: None,
            seq_progress: None,
        })
    }

    fn get_data(&self, subspace: &Subspace) -> Result<Element> {
        let element = self
            .kvs
            .get(&Wrapper(subspace.clone()))
            .ok_or(Error::SubspaceNotFound {
                subspace: subspace.clone(),
            })?
            .get(0)
            .ok_or(Error::EmptyElementSet)?
            .clone();
        Ok(element)
    }
}

impl<'de, 'a> serde::de::Deserializer<'de> for &'a mut Deserializer<'de> {
    type Error = Error;

    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let boolean_value = self
            .get_data(&subspace)?
            .as_bool()
            .ok_or(Error::NotABooleanValue {
                subspace: subspace.clone(),
            })?;
        visitor.visit_bool(boolean_value)
    }

    fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let i8_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as i8;
        visitor.visit_i8(i8_value)
    }

    fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let i8_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as i8;
        visitor.visit_i8(i8_value)
    }

    fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let i32_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as i32;
        visitor.visit_i32(i32_value)
    }

    fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let i64_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })?;
        visitor.visit_i64(i64_value)
    }

    fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let u8_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as u8;
        visitor.visit_u8(u8_value)
    }

    fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let u16_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as u16;
        visitor.visit_u16(u16_value)
    }

    fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let u32_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as u32;
        visitor.visit_u32(u32_value)
    }

    fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let u64_value = self
            .get_data(&subspace)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: subspace.clone(),
            })? as u64;
        visitor.visit_u64(u64_value)
    }

    fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let f32_value = self
            .get_data(&subspace)?
            .as_f32()
            .ok_or(Error::NotADoubleValue {
                subspace: subspace.clone(),
            })?;
        visitor.visit_f32(f32_value)
    }

    fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let f64_value = self
            .get_data(&subspace)?
            .as_f64()
            .ok_or(Error::NotADoubleValue {
                subspace: subspace.clone(),
            })?;
        visitor.visit_f64(f64_value)
    }

    fn deserialize_char<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        Err(Error::CharDeserializationNotSupported {
            subspace: self.subspace.subspace(&"_data").clone(),
        })
    }

    fn deserialize_str<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_string(visitor)
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let subspace = &self.subspace.subspace(&"_data");
        let str_value = self
            .get_data(&subspace)?
            .as_str()
            .ok_or(Error::NotStringValue {
                subspace: subspace.clone(),
            })?
            .to_string();
        visitor.visit_string(str_value)
    }

    fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_unit_struct<V>(self, name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_newtype_struct<V>(self, name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.subspace = self.subspace.subspace(&("_seq"));
        let old_subspace = self.subspace.clone();

        let key_len = &self.subspace.subspace(&("_len"));
        let len = self
            .get_data(key_len)?
            .as_i64()
            .ok_or(Error::NotAnIntegerValue {
                subspace: key_len.clone(),
            })? as usize;

        self.seq_progress = Some(SeqData::new(len));

        let value = visitor.visit_seq(SubspaceAccess::new(self));

        self.subspace = old_subspace;
        value
    }

    fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_tuple_struct<V>(
        self,
        name: &'static str,
        len: usize,
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_map<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let access = SubspaceAccess::new(self);
        visitor.visit_map(access)
    }

    fn deserialize_struct<V>(
        self,
        name: &'static str,
        fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let mut fields = fields
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>();

        fields.sort();

        self.subspace = self.subspace.subspace(&("_struct", name));

        self.struct_progress = Some(StructDataPartial {
            name: name.to_string(),
            fields,
            field: "".to_string(),
        });

        let result = self.deserialize_map(visitor);
        self.struct_progress = None;
        result
    }

    fn deserialize_enum<V>(
        self,
        name: &'static str,
        variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }

    fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        if let Some(struct_data) = self.struct_progress.as_mut() {
            return visitor.visit_str(struct_data.field.as_str());
        }

        unreachable!()
    }

    fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        todo!()
    }
}

struct SubspaceAccess<'a, 'de: 'a> {
    de: &'a mut Deserializer<'de>,
}

impl<'a, 'de> SubspaceAccess<'a, 'de> {
    fn new(de: &'a mut Deserializer<'de>) -> Self {
        SubspaceAccess { de }
    }
}

impl<'a, 'de> MapAccess<'de> for SubspaceAccess<'a, 'de> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: DeserializeSeed<'de>,
    {
        if let Some(struct_data) = self.de.struct_progress.as_mut() {
            return match struct_data.fields.pop() {
                None => Ok(None),
                Some(field) => {
                    struct_data.field = field.clone();
                    self.de.old_subspace = self.de.subspace.clone();
                    self.de.subspace = self.de.subspace.subspace(&("_field", field));
                    seed.deserialize(&mut *self.de).map(Some)
                }
            };
        }
        unreachable!()
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: DeserializeSeed<'de>,
    {
        let result = seed.deserialize(&mut *self.de);
        self.de.subspace = self.de.old_subspace.clone();
        result
    }
}

impl<'a, 'de> SeqAccess<'de> for SubspaceAccess<'a, 'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        if let Some(seq_data) = &mut self.de.seq_progress {
            if seq_data.is_finished() {
                return Ok(None);
            }

            let old_subspace = self.de.subspace.clone();

            self.de.subspace = seq_data.get_data_subspace(&self.de.subspace);
            seq_data.inc();

            let result = seed.deserialize(&mut *self.de).map(Some);
            self.de.subspace = old_subspace;
            return result;
        }

        Err(Error::SeqDataNotInitialized)
    }
}
