use foundationdb::tuple::Subspace;

#[derive(Debug)]
pub struct SeqData {
    len: usize,
    index: usize,
}

impl SeqData {
    pub fn new(len: usize) -> Self {
        SeqData { len, index: 0 }
    }

    pub fn inc(&mut self) {
        self.index += 1;
    }

    pub fn is_finished(&self) -> bool {
        self.index == self.len
    }

    pub fn get_data_subspace(&self, subspace: &Subspace) -> Subspace {
        subspace.subspace(&(self.index))
    }
}
