use crate::common::kvs::{create_new_type_struct_kvs, create_struct_kvs, create_tuple_struct_kvs};
use crate::common::types::DataFactory;
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;
use foundationdb_serde::ser::types::{Kind, StructData};
use serde::Serialize;
use std::collections::{BTreeMap, HashMap};

#[test]
fn test_serialize_simple_struct<'a>() {
    #[derive(Serialize)]
    struct ColorAlpha {
        value: u8,
    }

    let alpha = ColorAlpha { value: 1 };
    let fields = HashMap::from([("value".to_string(), DataFactory::from_data_u8(alpha.value))]);

    let subspace = Subspace::all();
    let result = to_keys_values(&alpha, &subspace).unwrap();
    let expected = create_struct_kvs(&StructData::from_data("ColorAlpha", fields), None);
    assert_eq!(expected, result);
}

#[test]
fn test_struct_multiple_field<'a>() {
    #[derive(Serialize)]
    struct Color {
        b: u8,
        g: u8,
        r: u8,
    }

    let color = Color {
        b: 254,
        g: 29,
        r: 45,
    };

    let fields = HashMap::from([
        ("g".to_string(), DataFactory::from_data_u8(color.g)),
        ("b".to_string(), DataFactory::from_data_u8(color.b)),
        ("r".to_string(), DataFactory::from_data_u8(color.r)),
    ]);

    let subspace = Subspace::all();
    let result = to_keys_values(&color, &subspace).unwrap();
    let expected = create_struct_kvs(&StructData::from_data("Color", fields), None);
    assert_eq!(expected, result);
}

#[test]
fn test_struct_multiple_fields_of_different_scalar_type<'a>() {
    #[derive(Serialize)]
    struct Human {
        name: String,
        age: u8,
        weight: f32,
        size: f64,
    }

    let human = Human {
        name: "Gabriel".to_string(),
        age: 99,
        weight: 75.0,
        size: 170.0,
    };

    let fields = HashMap::from([
        ("name".to_string(), DataFactory::from_data(&human.name)),
        ("age".to_string(), DataFactory::from_data_u8(human.age)),
        ("weight".to_string(), DataFactory::from_data(&human.weight)),
        ("size".to_string(), DataFactory::from_data(&human.size)),
    ]);

    let subspace = Subspace::all();
    let result = to_keys_values(&human, &subspace).unwrap();
    let expected = create_struct_kvs(&StructData::from_data("Human", fields), None);
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_simple_struct_with_prefix<'a>() {
    #[derive(Serialize)]
    struct ColorAlpha {
        value: u8,
    }

    let alpha = ColorAlpha { value: 1 };
    let fields = HashMap::from([("value".to_string(), DataFactory::from_data_u8(alpha.value))]);

    let subspace = Subspace::all().subspace(&("_user", &1254528285, "_app", "Redis Serverless"));
    let result = to_keys_values(&alpha, &subspace).unwrap();
    let expected = create_struct_kvs(
        &StructData::from_data("ColorAlpha", fields),
        Some(&subspace),
    );
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_struct_with_seq<'a>() {
    #[derive(Serialize)]
    struct SchoolMarks {
        values: Vec<f32>,
    }

    let values = vec![18.5, 13.0, 14.8, 12.3];
    let values_seq_data = DataFactory::seq_from_vec_data(&values);

    let marks = SchoolMarks { values };

    let fields = HashMap::from([("values".to_string(), Kind::Seq(values_seq_data))]);

    let subspace = Subspace::all();
    let result = to_keys_values(&marks, &subspace).unwrap();
    let expected = create_struct_kvs(&StructData::from_data("SchoolMarks", fields), None);
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_struct_with_multiple_seq<'a>() {
    #[derive(Serialize)]
    struct SchoolMarks {
        values: Vec<f32>,
        topics: Vec<&'static str>,
    }

    let values = vec![18.5, 13.0, 14.8, 12.3];
    let topics = vec!["French", "Arithmetics", "Sport", "Music"];

    let values_seq_data = DataFactory::seq_from_vec_data(&values);
    let topics_seq_data = DataFactory::seq_from_vec_data(&topics);

    let marks = SchoolMarks { values, topics };

    let fields = HashMap::from([
        ("values".to_string(), Kind::Seq(values_seq_data)),
        ("topics".to_string(), Kind::Seq(topics_seq_data)),
    ]);

    let subspace = Subspace::all();
    let result = to_keys_values(&marks, &subspace).unwrap();
    let expected = create_struct_kvs(&StructData::from_data("SchoolMarks", fields), None);
    assert_eq!(expected, result);
}

#[test]
fn test_struct_with_enum_unit_field() {
    #[derive(Serialize)]
    struct Human {
        name: String,
        gender: Gender,
    }

    #[derive(Serialize)]
    enum Gender {
        Male,
        Female,
        Other,
    }

    let human = Human {
        name: "Gabriel".to_string(),
        gender: Gender::Female,
    };

    let enum_data = DataFactory::enum_unit_from_data("Gender", "Female", 1);

    let fields = HashMap::from([
        ("name".to_string(), DataFactory::from_data(&human.name)),
        ("gender".to_string(), Kind::EnumVariantUnit(enum_data)),
    ]);

    let struct_data = StructData::from_data("Human", fields);

    let subspace = Subspace::all();
    let result = to_keys_values(&human, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_struct_with_enum_tuple_field() {
    #[derive(Serialize)]
    struct Ship {
        name: String,
        position_type: PositionType,
    }

    #[derive(Serialize)]
    enum PositionType {
        Cartesian(f64, u8),
    }

    let ship = Ship {
        name: "Gabriel".to_string(),
        position_type: PositionType::Cartesian(12.5, 12),
    };

    let enum_data = DataFactory::enum_tuple_from_data(
        "PositionType",
        "Cartesian",
        0,
        &vec![
            DataFactory::from_data(&(12.5 as f64)),
            DataFactory::from_data_u8(12),
        ],
    );

    let fields = HashMap::from([
        ("name".to_string(), DataFactory::from_data(&ship.name)),
        (
            "position_type".to_string(),
            Kind::EnumVariantTuple(enum_data),
        ),
    ]);

    let struct_data = StructData::from_data("Ship", fields);

    let subspace = Subspace::all();
    let result = to_keys_values(&ship, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_struct_with_map_field() {
    #[derive(Serialize)]
    struct Travels {
        name: String,
        locations: BTreeMap<u64, String>,
    }

    let locations = BTreeMap::from([
        (1992 as u64, "Brest".to_string()),
        (1995 as u64, "Pékin".to_string()),
        (2001 as u64, "New York".to_string()),
    ]);

    let map_data = DataFactory::map_from_hash_map_data_key_data_value(&locations);

    let travel = Travels {
        name: "Gabriel".to_string(),
        locations,
    };

    let fields = HashMap::from([
        ("name".to_string(), DataFactory::from_data(&travel.name)),
        ("locations".to_string(), Kind::Map(map_data)),
    ]);

    let subspace = Subspace::all();
    let result = to_keys_values(&travel, &subspace).unwrap();
    let expected = create_struct_kvs(&StructData::from_data("Travels", fields), None);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_nested_struct() {
    #[derive(Serialize)]
    struct Color {
        r: u8,
        g: u8,
        b: u8,
        a: ColorAlpha,
    }

    #[derive(Serialize)]
    struct ColorAlpha {
        value: u8,
    }

    let color = Color {
        r: 45,
        g: 29,
        b: 254,
        a: ColorAlpha { value: 45 },
    };

    let color_alpha_struct = StructData::from_data(
        "ColorAlpha",
        HashMap::from([("value".to_string(), DataFactory::from_data_u8(45))]),
    );

    let color_fields = HashMap::from([
        ("r".to_string(), DataFactory::from_data_u8(color.r)),
        ("g".to_string(), DataFactory::from_data_u8(color.g)),
        ("b".to_string(), DataFactory::from_data_u8(color.b)),
        ("a".to_string(), Kind::Struct(color_alpha_struct)),
    ]);

    let struct_data = StructData::from_data("Color", color_fields);

    let subspace = Subspace::all();
    let result = to_keys_values(&color, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_struct_with_tuple_field() {
    #[derive(Serialize)]
    struct Point {
        position: (f32, f32, f64),
    }

    let tuple = (1552.452, 45238.3, 455652.56353526336);

    let tuple_data = DataFactory::tuple_from_vec(&vec![
        DataFactory::from_data(&tuple.0),
        DataFactory::from_data(&tuple.1),
        DataFactory::from_data(&tuple.2),
    ]);

    let point = Point { position: tuple };

    let struct_data = StructData::from_data(
        "Point",
        HashMap::from([("position".to_string(), Kind::Tuple(tuple_data))]),
    );

    let subspace = Subspace::all();
    let result = to_keys_values(&point, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_struct_with_unit_struct_field() {
    #[derive(Serialize)]
    struct Void;

    #[derive(Serialize)]
    struct Oblivion {
        void: Void,
    }

    let oblivion = Oblivion { void: Void };

    let struct_data = StructData::from_data(
        "Oblivion",
        HashMap::from([("void".to_string(), Kind::UnitStruct("Void".to_string()))]),
    );

    let subspace = Subspace::all();
    let result = to_keys_values(&oblivion, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_struct_with_enum_struct_field() {
    #[derive(Serialize)]
    struct Pixel {
        sat: Color,
        color: Color,
    }

    #[derive(Serialize)]
    enum Color {
        GrayScale(u8),
        RGB(u8, u8, u8),
    }

    let color = Pixel {
        color: Color::RGB(12, 45, 79),
        sat: Color::GrayScale(15),
    };

    let enum_tuple_data = DataFactory::enum_tuple_from_data(
        "Color",
        "RGB",
        1,
        &vec![
            DataFactory::from_data_u8(12),
            DataFactory::from_data_u8(45),
            DataFactory::from_data_u8(79),
        ],
    );

    let enum_unit_data = DataFactory::enum_new_type_from_data(
        "Color",
        "GrayScale",
        0,
        DataFactory::from_data_u8(15),
    );

    let struct_data = StructData::from_data(
        "Pixel",
        HashMap::from([
            ("color".to_string(), Kind::EnumVariantTuple(enum_tuple_data)),
            ("sat".to_string(), Kind::EnumVariantNewType(enum_unit_data)),
        ]),
    );

    let subspace = Subspace::all();
    let result = to_keys_values(&color, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_struct_with_enum_struct_variant_field() {
    #[derive(Serialize)]
    struct Pixel {
        color: Color,
    }

    #[derive(Serialize)]
    enum Color {
        Rgb { r: u8, g: u8, b: u8 },
    }

    let color = Pixel {
        color: Color::Rgb {
            r: 45,
            g: 83,
            b: 72,
        },
    };

    let fields = HashMap::from([
        ("r".to_string(), DataFactory::from_data_u8(45)),
        ("g".to_string(), DataFactory::from_data_u8(83)),
        ("b".to_string(), DataFactory::from_data_u8(72)),
    ]);

    let struct_data_rgb = StructData::from_data("Rgb", fields);

    let enum_data = DataFactory::enum_struct_from_data("Color", "Rgb", 0, struct_data_rgb);

    let struct_data = StructData::from_data(
        "Pixel",
        HashMap::from([("color".to_string(), Kind::EnumVariantStruct(enum_data))]),
    );

    let subspace = Subspace::all();
    let result = to_keys_values(&color, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_struct_with_new_type_field() {
    #[derive(Serialize)]
    struct Meters(f64);

    #[derive(Serialize)]
    struct Seconds(u8);

    #[derive(Serialize)]
    struct Dimensions {
        width: Meters,
        height: Meters,
        time: Seconds,
    }

    let dimensions = Dimensions {
        width: Meters(12.45),
        height: Meters(430.12358),
        time: Seconds(45),
    };

    let struct_data = StructData::from_data(
        "Dimensions",
        HashMap::from([
            (
                "width".to_string(),
                Kind::NewTypeStruct(DataFactory::type_struct_from_data(
                    "Meters",
                    &DataFactory::from_data(&dimensions.width.0),
                )),
            ),
            (
                "height".to_string(),
                Kind::NewTypeStruct(DataFactory::type_struct_from_data(
                    "Meters",
                    &DataFactory::from_data(&dimensions.height.0),
                )),
            ),
            (
                "time".to_string(),
                Kind::NewTypeStruct(DataFactory::type_struct_from_data(
                    "Seconds",
                    &DataFactory::from_data_u8(dimensions.time.0),
                )),
            ),
        ]),
    );

    let subspace = Subspace::all();
    let result = to_keys_values(&dimensions, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_struct_with_tuple_struct_field() {
    #[derive(Serialize)]
    struct Pixel {
        position: Position,
    }

    #[derive(Serialize)]
    struct Position(f32, f64);

    let position = Position(145.43, 78.21);

    let struct_data = StructData::from_data(
        "Pixel",
        HashMap::from([(
            "position".to_string(),
            Kind::TupleStruct(DataFactory::tuple_struct_from_data(
                "Position",
                vec![
                    DataFactory::from_data(&position.0),
                    DataFactory::from_data(&position.1),
                ],
            )),
        )]),
    );

    let pixel = Pixel { position };

    let subspace = Subspace::all();
    let result = to_keys_values(&pixel, &subspace).unwrap();
    let expected = create_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_new_type_struct() {
    #[derive(Serialize)]
    struct Meter(f64);

    let data = Meter(4562.6632);
    let subspace = Subspace::all();

    let struct_data = DataFactory::type_struct_from_data("Meter", &DataFactory::from_data(&data.0));

    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_new_type_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_tuple_struct() {
    #[derive(Serialize)]
    struct RGB(u8, u8, u8);

    let data = RGB(152, 45, 13);
    let subspace = Subspace::all();

    let struct_data = DataFactory::tuple_struct_from_data(
        "RGB",
        vec![
            DataFactory::from_data_u8(data.0),
            DataFactory::from_data_u8(data.1),
            DataFactory::from_data_u8(data.2),
        ],
    );

    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_tuple_struct_kvs(&struct_data, None);
    assert_eq!(expected, result)
}
