use crate::common::kvs::{create_data_field, create_key_value, create_keys_values};
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;

#[test]
fn test_serialize_none() {
    let data: Option<u8> = None;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &());
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_some() {
    let data: Option<u8> = Some(45);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &(data.unwrap() as u16));
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}
