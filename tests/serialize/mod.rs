mod collections;
mod enumeration;
mod options;
mod scalar;
mod structures;

use crate::common::kvs::create_struct_kvs;
use crate::common::types::DataFactory;
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;
use foundationdb_serde::ser::types::{Kind, StructData};
use serde::Serialize;
use std::collections::{BTreeMap, HashMap, HashSet};

#[test]
fn various() {
    #[derive(Serialize)]
    struct Void;

    #[derive(Serialize)]
    struct Meter(u8);

    #[derive(Serialize)]
    struct RGB(u8, u8, u8);

    #[derive(Serialize)]
    struct HSL {
        h: u8,
        s: u8,
        l: u8,
    }

    #[derive(Serialize)]
    enum ColorKind {
        RGB(RGB),
        HSL(HSL),
        HSV { h: u8, s: u8, v: u8 },
    }

    #[derive(Serialize)]
    struct Toto {
        a: u8,
        b: f64,
        option_none: Option<u8>,
        option_some: Option<u8>,
        option_struct: Option<Tutu>,
        tata: Tata,
        void: Void,
        length: Meter,
        array: Vec<i16>,
        tuple: (u16, f64, bool, Tutu, char, &'static str),
        rgb: RGB,
        map: HashMap<String, u32>,
        color_kind: ColorKind,
        color_kind2: ColorKind,
        color_kind3: ColorKind,
    }

    #[derive(Serialize)]
    struct Tata {
        c: u8,
        titi: Titi,
    }

    #[derive(Serialize)]
    struct Titi {
        d: bool,
        color: Color,
        color_custom: Color,
        color_tutu: Color,
    }

    #[derive(Serialize)]
    struct Tutu {
        test: bool,
    }

    #[derive(Serialize)]
    enum Color {
        Red,
        Custom(u8, u8, u8),
        Tutu(Tutu),
    }

    let toto = Toto {
        a: 12,
        b: 4.5,
        option_none: None,
        option_some: Some(12),
        option_struct: Some(Tutu { test: false }),
        void: Void,
        length: Meter(40),
        array: vec![-1, 12, -48],
        tuple: (45, 5.565, false, Tutu { test: false }, 'c', "hello world"),
        rgb: RGB(10, 25, 156),
        map: HashMap::from([("Test".to_owned(), 42), ("Test2".to_owned(), 66)]),
        color_kind: ColorKind::RGB(RGB(1, 1, 1)),
        color_kind2: ColorKind::HSL(HSL {
            h: 45,
            s: 12,
            l: 78,
        }),
        color_kind3: ColorKind::HSV {
            h: 45,
            s: 83,
            v: 14,
        },
        tata: Tata {
            c: 45,
            titi: Titi {
                d: true,
                color: Color::Red,
                color_custom: Color::Custom(1, 45, 136),
                color_tutu: Color::Tutu(Tutu { test: true }),
            },
        },
    };
    let subspace = Subspace::all().subspace(&("test_serde"));

    let result = to_keys_values(&toto, &subspace);
    assert_eq!(true, result.is_ok())
}

#[test]
fn test_serialize_redis_value_with_skipped_field() {
    #[derive(Serialize)]
    struct RedisValue {
        #[serde(skip)]
        key: String,
        value: RedisValueKind,
    }

    #[derive(Serialize)]
    enum RedisValueKind {
        String(String),
        Set(HashSet<String>),
        List(Vec<String>),
        Map(BTreeMap<String, String>),
    }

    let map_data = DataFactory::map_from_hash_map_data_key_data_value(&BTreeMap::from([
        (&"Albert", &"10"),
        (&"Camille", &"15"),
    ]));

    let struct_data = StructData::from_data(
        "RedisValue",
        HashMap::from([
            // ("key".to_string(), Kind::from_data(&"family")),
            (
                "value".to_string(),
                Kind::EnumVariantNewType(DataFactory::enum_new_type_from_data(
                    "RedisValueKind",
                    "Map",
                    3,
                    Kind::Map(map_data),
                )),
            ),
        ]),
    );

    let redis_value = RedisValue {
        key: "family".to_string(),
        value: RedisValueKind::Map(BTreeMap::from([
            ("Albert".to_string(), "10".to_string()),
            ("Camille".to_string(), "15".to_string()),
        ])),
    };

    let subspace = Subspace::all().subspace(&("family"));
    let expected = create_struct_kvs(&struct_data, Some(&subspace));
    let result = to_keys_values(&redis_value, &subspace).unwrap();
    assert_eq!(expected, result)
}
