use crate::common::kvs::{
    create_enum_new_type_kvs, create_enum_struct_kvs, create_enum_tuple_kvs, create_enum_unit_kvs,
};
use crate::common::types::DataFactory;
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;
use foundationdb_serde::ser::types::{Kind, StructData};
use serde::Serialize;
use std::collections::HashMap;

#[test]
fn test_serialize_enum_unit_variant() {
    #[derive(Serialize)]
    enum Choice {
        One,
    }

    let data = Choice::One;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected =
        create_enum_unit_kvs(&DataFactory::enum_unit_from_data("Choice", "One", 0), None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_enum_new_type_variant() {
    #[derive(Serialize)]
    enum Color {
        RGB(RGB),
    }

    #[derive(Serialize)]
    struct RGB {
        r: u8,
        g: u8,
        b: u8,
    }

    let color_struct_data = RGB {
        r: 15,
        g: 73,
        b: 156,
    };

    let fields = HashMap::from([
        (
            "r".to_string(),
            DataFactory::from_data_u8(color_struct_data.r),
        ),
        (
            "g".to_string(),
            DataFactory::from_data_u8(color_struct_data.g),
        ),
        (
            "b".to_string(),
            DataFactory::from_data_u8(color_struct_data.b),
        ),
    ]);

    let struct_data = StructData::from_data("RGB", fields);

    let data = Color::RGB(color_struct_data);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_enum_new_type_kvs(
        &DataFactory::enum_new_type_from_data("Color", "RGB", 0, Kind::Struct(struct_data)),
        None,
    );
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_enum_struct_variant() {
    #[derive(Serialize)]
    enum Color {
        RGB { r: u8, g: u8, b: u8 },
    }

    let data = Color::RGB {
        r: 15,
        g: 73,
        b: 156,
    };

    let struct_data = StructData::from_data(
        "RGB",
        HashMap::from([
            ("r".to_string(), DataFactory::from_data_u8(15)),
            ("g".to_string(), DataFactory::from_data_u8(73)),
            ("b".to_string(), DataFactory::from_data_u8(156)),
        ]),
    );

    let enum_data = DataFactory::enum_struct_from_data("Color", "RGB", 0, struct_data);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_enum_struct_kvs(&enum_data, None);
    assert_eq!(expected, result)
}

#[test]
fn test_serialize_enum_tuple_variant() {
    #[derive(Serialize)]
    enum Color {
        RGB(u8, u8, u8),
    }

    let data = Color::RGB(15, 73, 156);
    let enum_tuple_data = DataFactory::enum_tuple_from_data(
        "Color",
        "RGB",
        0,
        &vec![
            DataFactory::from_data_u8(15),
            DataFactory::from_data_u8(73),
            DataFactory::from_data_u8(156),
        ],
    );

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_enum_tuple_kvs(&enum_tuple_data, None);
    assert_eq!(expected, result);
}
