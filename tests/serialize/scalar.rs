use crate::common::kvs::{create_data_field, create_key_value, create_keys_values};
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;

#[test]
fn test_serialize_bool() {
    let data = true;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_i8() {
    let data = i8::MAX as i8;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &(data as i16));
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_i16() {
    let data = u16::MAX as i16;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_i32() {
    let data = i32::MAX as i32;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_i64() {
    let data = i64::MAX as i64;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_u8() {
    let data = u8::MAX as u8;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &(data as u16));
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_u16() {
    let data = u16::MAX as u16;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_u32() {
    let data = u32::MAX as u32;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_u64() {
    let data = u64::MAX as u64;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

// #[test]
// fn test_serialize_u128() {
//     let data = u128::MAX as u128;
//
//     let subspace = Subspace::all();
//     let result = to_keys_values(&data, &subspace);
//     assert_eq!(true, result.is_err())
// }

#[test]
fn test_serialize_f32() {
    let data = f32::MAX as f32;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_f64() {
    let data = f64::MAX as f64;

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_char() {
    let data = 'a';

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data.to_string());
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_str() {
    let data = "a";

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}

#[test]
fn test_serialize_string() {
    let data = "a".to_string();

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let kv = create_key_value(create_data_field(None), &data);
    let expected = create_keys_values(vec![kv]);

    assert_eq!(expected, result)
}
