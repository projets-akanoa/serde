use crate::common::kvs::{create_map_kvs, create_seq_kvs, create_tuple_kvs};
use crate::common::types::DataFactory;
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;
use foundationdb_serde::ser::types::{
    Kind, SeqData, StructData, TupleData, TupleStructData, TypeStructData,
};
use serde::Serialize;
use std::collections::{BTreeMap, HashMap};

#[test]
// todo : Found how to trigger the serialization
fn test_serialize_bytes_array() {
    let bytes: [u8; 2] = [0, 8];

    let subspace = Subspace::all();
    let result = to_keys_values(&bytes, &subspace).unwrap();
    //println!("{}", result)
}

#[test]
fn test_serialize_vec() {
    let data: Vec<u32> = vec![45, 49, 36];
    let seq_data = DataFactory::seq_from_vec_data(&data);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_seq_kvs(&seq_data, None);
    assert_eq!(expected, result)
}

#[test]
// todo: found a way to assert tuple serialization
fn test_serialize_tuple() {
    let data = (78, true, "chat", 7252525.456, (45, 'a'));

    let tuple_data = DataFactory::tuple_from_vec(&vec![
        DataFactory::from_data(&data.0),
        DataFactory::from_data(&data.1),
        DataFactory::from_data(&data.2),
        DataFactory::from_data(&data.3),
        Kind::Tuple(DataFactory::tuple_from_vec(&vec![
            DataFactory::from_data(&data.4 .0),
            DataFactory::from_data_char(data.4 .1),
        ])),
    ]);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_tuple_kvs(&tuple_data, Some(&subspace));
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_tuple_with_struct() {
    #[derive(Serialize)]
    struct Meter(u8);

    #[derive(Serialize)]
    struct Second(u8);

    #[derive(Serialize)]
    struct Dimensions {
        width: Meter,
        height: Meter,
        time: Second,
    }

    #[derive(Serialize)]
    struct RGB(u8, u8, u8);

    let data = (
        78,
        true,
        "chat",
        7252525.456,
        (45, 'a'),
        Meter(45),
        Dimensions {
            width: Meter(45),
            height: Meter(78),
            time: Second(12),
        },
        RGB(15, 78, 39),
    );

    let fields = HashMap::from([
        (
            "width".to_string(),
            Kind::NewTypeStruct(DataFactory::type_struct_from_data(
                "Meter",
                &DataFactory::from_data_u8(data.6.width.0),
            )),
        ),
        (
            "height".to_string(),
            Kind::NewTypeStruct(DataFactory::type_struct_from_data(
                "Meter",
                &DataFactory::from_data_u8(data.6.height.0),
            )),
        ),
        (
            "time".to_string(),
            Kind::NewTypeStruct(DataFactory::type_struct_from_data(
                "Second",
                &DataFactory::from_data_u8(data.6.time.0),
            )),
        ),
    ]);

    let dimensions_struct = StructData::from_data("Dimensions", fields);
    let rgb_struct = DataFactory::tuple_struct_from_data(
        "RGB",
        vec![
            DataFactory::from_data_u8(data.7 .0),
            DataFactory::from_data_u8(data.7 .1),
            DataFactory::from_data_u8(data.7 .2),
        ],
    );

    let tuple_data = DataFactory::tuple_from_vec(&vec![
        DataFactory::from_data(&data.0),
        DataFactory::from_data(&data.1),
        DataFactory::from_data(&data.2),
        DataFactory::from_data(&data.3),
        Kind::Tuple(DataFactory::tuple_from_vec(&vec![
            DataFactory::from_data(&data.4 .0),
            DataFactory::from_data_char(data.4 .1),
        ])),
        Kind::NewTypeStruct(DataFactory::type_struct_from_data(
            "Meter",
            &DataFactory::from_data_u8(data.5 .0),
        )),
        Kind::Struct(dimensions_struct),
        Kind::TupleStruct(rgb_struct),
    ]);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_tuple_kvs(&tuple_data, Some(&subspace));
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_map() {
    let data: BTreeMap<String, u16> =
        BTreeMap::from([("Albert".to_string(), 12), ("Camille".to_string(), 13)]);

    let map_data = DataFactory::map_from_hash_map_data_key_data_value(&data);

    let subspace = Subspace::all();
    let result = to_keys_values(&data, &subspace).unwrap();
    let expected = create_map_kvs(&map_data, None);
    assert_eq!(expected, result);
}
