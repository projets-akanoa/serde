use foundationdb::tuple::{pack, TuplePack};
use foundationdb_serde::ser::types::{
    EnumNewTypeVariantData, EnumStructVariantData, EnumTupleVariantData, EnumUnitVariantData, Kind,
    MapData, SeqData, StructData, TupleData, TupleStructData, TypeStructData,
};
use std::collections::{BTreeMap, HashMap};
use std::fmt::Debug;
use std::hash::Hash;

pub struct DataFactory;

impl DataFactory {
    pub fn from_data<T: TuplePack>(value: &T) -> Kind {
        Kind::Data(pack(value))
    }

    pub fn from_data_u8(value: u8) -> Kind {
        Kind::Data(pack(&(value as u16)))
    }

    pub fn from_data_char(value: char) -> Kind {
        Kind::Data(pack(&(value.to_string())))
    }

    pub fn struct_from_data(name: &str, fields: HashMap<String, Kind>) -> StructData {
        let fields_str = fields.keys().map(|x| x.clone()).collect();

        StructData {
            name: name.to_string(),
            len: fields.len(),
            value: fields,
            fields: fields_str,
        }
    }

    pub fn tuple_struct_from_data(name: &str, value: Vec<Kind>) -> TupleStructData {
        TupleStructData {
            name: name.to_string(),
            len: value.len(),
            value,
        }
    }

    pub fn type_struct_from_data(name: &str, value: &Kind) -> TypeStructData {
        TypeStructData {
            name: name.to_string(),
            value: Box::new(value.clone()),
        }
    }

    pub fn enum_unit_from_data<'a>(
        name: &'a str,
        variant: &'a str,
        index: u32,
    ) -> EnumUnitVariantData {
        EnumUnitVariantData {
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index: index,
        }
    }

    pub fn enum_new_type_from_data<'a>(
        name: &'a str,
        variant: &'a str,
        index: u32,
        value: Kind,
    ) -> EnumNewTypeVariantData {
        EnumNewTypeVariantData {
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index: index,
            value: Box::new(value),
        }
    }

    pub fn enum_struct_from_data<'a>(
        name: &'a str,
        variant: &'a str,
        index: u32,
        value: StructData,
    ) -> EnumStructVariantData {
        EnumStructVariantData {
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index: index,
            value,
        }
    }

    pub fn enum_tuple_from_data<'a>(
        name: &'a str,
        variant: &'a str,
        index: u32,
        vec: &Vec<Kind>,
    ) -> EnumTupleVariantData {
        EnumTupleVariantData {
            value: vec.clone(),
            name: name.to_string(),
            variant: variant.to_string(),
            variant_index: index,
            len: vec.len(),
        }
    }

    pub fn tuple_from_vec(vec: &Vec<Kind>) -> TupleData {
        TupleData {
            value: vec.clone(),
            len: vec.len(),
        }
    }

    pub fn seq_from_vec_data<T: TuplePack + Debug>(vec: &Vec<T>) -> SeqData {
        let value = vec.iter().fold(vec![], |mut acc, e| {
            acc.push(Kind::Data(pack(e)));
            acc
        });

        SeqData {
            value,
            len: Some(vec.len()),
        }
    }

    pub fn map_from_hash_map_data_key_data_value<K: TuplePack + Hash + Eq, V: TuplePack>(
        map: &BTreeMap<K, V>,
    ) -> MapData {
        let value = map.iter().fold(vec![], |mut acc, (key, value)| {
            acc.push((Kind::Data(pack(key)), Kind::Data(pack(value))));
            acc
        });

        MapData {
            len: Some(value.len()),
            value,
        }
    }
}
