use crate::common::types::DataFactory;
use foundationdb::tuple::{pack, Subspace, TuplePack};
use foundationdb_serde::ser::types::{
    EnumNewTypeVariantData, EnumStructVariantData, EnumTupleVariantData, EnumUnitVariantData, Kind,
    MapData, SeqData, StructData, TupleData, TupleStructData, TypeStructData,
};
use foundationdb_serde::{KeyValue, KeysValues};
use std::fmt::Debug;
use std::ops::Deref;

pub fn create_key_value<T: TuplePack + Debug>(key: Vec<u8>, value: &T) -> KeyValue {
    (key, pack(&value))
}

pub fn create_keys_values(kvs: Vec<KeyValue>) -> KeysValues {
    KeysValues { inner: kvs }
}

pub fn create_data_field(subspace: Option<&Subspace>) -> Vec<u8> {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    subspace.subspace(&("_data")).into_bytes()
}

pub fn create_tuple_kvs(tuple: &TupleData, subspace: Option<&Subspace>) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_tuple"));

    let mut out = vec![];
    let len = tuple.len;
    out.push((subspace.pack(&("_len")), pack(&len)));

    let mut i = 0;
    tuple.value.iter().for_each(|value| {
        let subspace = subspace.subspace(&(i));
        match value {
            Kind::Data(value) => {
                let key = subspace.subspace(&("_data")).into_bytes();
                out.push((key, value.clone()));
            }
            Kind::Tuple(tuple_data) => {
                let kvs = create_tuple_kvs(tuple_data, Some(&subspace));
                out.extend(kvs.inner);
            }
            Kind::Struct(struct_data) => {
                let kvs = create_struct_kvs(&struct_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::NewTypeStruct(type_struct_data) => {
                let kvs = create_new_type_struct_kvs(type_struct_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::TupleStruct(tuple_struct) => {
                let kvs = create_tuple_struct_kvs(tuple_struct, Some(&subspace));
                out.extend(kvs.inner)
            }
            _ => unreachable!(),
        }

        i += 1;
    });

    KeysValues { inner: out }
}

pub fn create_seq_kvs(seq: &SeqData, subspace: Option<&Subspace>) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_seq"));

    let mut out = vec![];
    let len = seq.len.unwrap();
    out.push((subspace.pack(&("_len")), pack(&len)));

    let mut i = 0;
    seq.value.iter().for_each(|value| {
        match value {
            Kind::Data(value) => {
                let key = subspace.subspace(&(i, "_data")).into_bytes();
                out.push((key, value.clone()));
            }
            _ => unreachable!(),
        }

        i += 1;
    });

    KeysValues { inner: out }
}

pub fn create_map_kvs(map: &MapData, subspace: Option<&Subspace>) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_map"));

    let mut out = vec![];
    let len = map.len.unwrap();
    out.push((subspace.pack(&("_len")), pack(&len)));

    let mut i = 0;

    for (key, value) in &map.value {
        //let subspace = subspace.subspace(&(i));

        let key = match key {
            Kind::Data(key) => key,
            _ => unreachable!(),
        };

        let value = match value {
            Kind::Data(value) => value,
            _ => unreachable!(),
        };

        let key_field = subspace.subspace(&("_key", i, "_data")).into_bytes();
        let value_field = subspace.subspace(&("_value", i, "_data")).into_bytes();
        out.push((key_field, key.clone()));
        out.push((value_field, value.clone()));
        i += 1;
    }

    KeysValues { inner: out }
}

pub fn create_new_type_struct_kvs<'a>(
    type_struct: &TypeStructData,
    subspace: Option<&Subspace>,
) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_struct", type_struct.name.clone()));

    let mut out = vec![];

    match type_struct.value.deref() {
        Kind::Data(data) => {
            let key = create_data_field(Some(&subspace));
            let kv = (key, data.clone());
            out.push(kv)
        }
        _ => unreachable!(),
    }

    KeysValues { inner: out }
}

pub fn create_tuple_struct_kvs(
    tuple_struct: &TupleStructData,
    subspace: Option<&Subspace>,
) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_struct", tuple_struct.name.clone()));

    let mut out = vec![];
    let kvs = create_tuple_kvs(
        &DataFactory::tuple_from_vec(&tuple_struct.value.clone()),
        Some(&subspace),
    );
    out.extend(kvs.inner);

    KeysValues { inner: out }
}

pub fn create_struct_kvs<'a>(struct_data: &StructData, subspace: Option<&Subspace>) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_struct", &struct_data.name));

    let mut out = vec![];

    struct_data.value.clone().iter().for_each(|(field, value)| {
        let subspace = subspace.subspace(&("_field", field));

        match value {
            Kind::Data(value) => {
                let field = subspace.subspace(&("_data")).into_bytes();
                out.push((field, value.clone()));
            }
            Kind::Seq(seq_data) => {
                let kvs = create_seq_kvs(&seq_data, Some(&subspace));
                out.extend(kvs.inner);
            }
            Kind::Map(map_data) => {
                let kvs = create_map_kvs(&map_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::Tuple(tuple_data) => {
                let kvs = create_tuple_kvs(&tuple_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::Struct(struct_data) => {
                let kvs = create_struct_kvs(&struct_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::TupleStruct(tuple_struct_data) => {
                let kvs = create_tuple_struct_kvs(tuple_struct_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::EnumVariantUnit(enum_data) => {
                let kvs = create_enum_unit_kvs(enum_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::EnumVariantTuple(enum_data) => {
                let kvs = create_enum_tuple_kvs(enum_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::EnumVariantStruct(enum_data) => {
                let kvs = create_enum_struct_kvs(enum_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::EnumVariantNewType(enum_data) => {
                let kvs = create_enum_new_type_kvs(enum_data, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::NewTypeStruct(type_struct) => {
                let kvs = create_new_type_struct_kvs(&type_struct, Some(&subspace));
                out.extend(kvs.inner)
            }
            Kind::UnitStruct(name) => {
                let key = subspace.pack(&("_unit_struct", name));
                out.push((key, vec![]))
            }
        };
    });

    KeysValues { inner: out }
}

pub fn create_enum_unit_kvs(
    enum_data: &EnumUnitVariantData,
    subspace: Option<&Subspace>,
) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let key = subspace.pack(&("_enum", &enum_data.name, &enum_data.variant));
    let out = vec![(key, vec![])];
    KeysValues { inner: out }
}

pub fn create_enum_new_type_kvs(
    enum_data: &EnumNewTypeVariantData,
    subspace: Option<&Subspace>,
) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_enum", &enum_data.name, &enum_data.variant));
    let mut out = vec![];

    match &enum_data.value.deref() {
        Kind::Data(data) => {
            let key = create_data_field(Some(&subspace));
            out.push((key, data.clone()))
        }
        Kind::Struct(struct_data) => {
            let kvs = create_struct_kvs(struct_data, Some(&subspace));
            out.extend(kvs.inner);
        }
        Kind::Map(map_data) => {
            let kvs = create_map_kvs(map_data, Some(&subspace));
            out.extend(kvs.inner);
        }
        _ => unreachable!(),
    };

    KeysValues { inner: out }
}

pub fn create_enum_tuple_kvs(
    enum_data: &EnumTupleVariantData,
    subspace: Option<&Subspace>,
) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_enum", &enum_data.name, &enum_data.variant));
    let mut out = vec![];

    let tuple_data = &DataFactory::tuple_from_vec(&enum_data.value.clone());

    let kvs = create_tuple_kvs(&tuple_data, Some(&subspace));
    out.extend(kvs.inner);

    KeysValues { inner: out }
}

pub fn create_enum_struct_kvs(
    enum_data: &EnumStructVariantData,
    subspace: Option<&Subspace>,
) -> KeysValues {
    let default = Subspace::all();
    let subspace = subspace.unwrap_or(&default);
    let subspace = subspace.subspace(&("_enum", &enum_data.name, &enum_data.variant));
    let mut out = vec![];

    let kvs = create_struct_kvs(&enum_data.value, Some(&subspace));
    out.extend(kvs.inner);

    KeysValues { inner: out }
}
