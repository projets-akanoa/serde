use crate::common::kvs::create_struct_kvs;
use crate::common::types::DataFactory;
use foundationdb_serde::de::from_keys_values;
use foundationdb_serde::ser::types::StructData;
use serde::Deserialize;
use std::collections::HashMap;
use foundationdb::tuple::Subspace;
use foundationdb_serde::ser::to_keys_values;

#[test]
fn test_basic_structure() {
    #[derive(Deserialize, PartialEq, Debug)]
    struct ColorAlpha {
        value: u8,
    }

    let expected = ColorAlpha { value: 12 };

    let struct_data = StructData::from_data(
        "ColorAlpha",
        HashMap::from([(
            "value".to_string(),
            DataFactory::from_data_u8(expected.value),
        )]),
    );

    let kvs = create_struct_kvs(&struct_data, None);
    let result = from_keys_values::<ColorAlpha>(&kvs, None).unwrap();

    assert_eq!(expected, result)
}

#[test]
fn test_multiple_fields_structure() {
    #[derive(Deserialize, PartialEq, Debug)]
    struct Color {
        r: u8,
        b: u8,
        g: u8,
    }

    let expected = Color {
        r: 42,
        b: 39,
        g: 15,
    };

    let struct_data = StructData::from_data(
        "Color",
        HashMap::from([
            ("r".to_string(), DataFactory::from_data_u8(expected.r)),
            ("g".to_string(), DataFactory::from_data_u8(expected.g)),
            ("b".to_string(), DataFactory::from_data_u8(expected.b)),
        ]),
    );

    let kvs = create_struct_kvs(&struct_data, None);
    let result = from_keys_values::<Color>(&kvs, None).unwrap();

    assert_eq!(expected, result)
}

#[test]
fn test_struct_multiple_fields_of_different_scalar_type<'a>() {
    #[derive(Deserialize, PartialEq, Debug)]
    struct Human {
        name: String,
        age: u8,
        weight: f32,
        size: f64,
    }

    let expected = Human {
        name: "Gabriel".to_string(),
        age: 99,
        weight: 75.0,
        size: 170.0,
    };

    let fields = HashMap::from([
        ("name".to_string(), DataFactory::from_data(&expected.name)),
        ("age".to_string(), DataFactory::from_data_u8(expected.age)),
        ("weight".to_string(), DataFactory::from_data(&expected.weight)),
        ("size".to_string(), DataFactory::from_data(&expected.size)),
    ]);

    let kvs = create_struct_kvs(&StructData::from_data("Human", fields), None);
    let result = from_keys_values(&kvs, None).unwrap();
    assert_eq!(expected, result);
}

#[test]
fn test_serialize_simple_struct_with_prefix<'a>() {
    #[derive(Deserialize, PartialEq, Debug)]
    struct ColorAlpha {
        value: u8,
    }

    let expected = ColorAlpha { value: 1 };
    let fields = HashMap::from([("value".to_string(), DataFactory::from_data_u8(expected.value))]);

    let subspace = Subspace::all().subspace(&("_user", &1254528285, "_app", "Redis Serverless"));
    let kvs = create_struct_kvs(
        &StructData::from_data("ColorAlpha", fields),
        Some(&subspace),
    );
    let result = from_keys_values::<ColorAlpha>(&kvs, Some(&subspace)).unwrap();
    assert_eq!(expected, result);
}
