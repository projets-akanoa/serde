use crate::common::kvs::create_seq_kvs;
use crate::common::types::DataFactory;
use foundationdb_serde::de::from_keys_values;

#[test]
fn test_deserialize_vec() {
    let data: Vec<u32> = vec![45, 49, 36];
    let seq_data = DataFactory::seq_from_vec_data(&data);

    let kvs = create_seq_kvs(&seq_data, None);
    let result = from_keys_values::<Vec<u32>>(&kvs, None).unwrap();
    assert_eq!(data, result)
}
