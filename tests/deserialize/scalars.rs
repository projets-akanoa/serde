use crate::common::kvs::{create_data_field, create_key_value};
use foundationdb::tuple::{pack, Subspace};
use foundationdb_serde::de::from_keys_values;
use foundationdb_serde::errors::Error;
use foundationdb_serde::KeysValues;

#[test]
fn test_deserialize_bool() {
    let data = false;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<bool>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_i8() {
    let data = i8::MAX as i8;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &(data as i16));
    let result = from_keys_values::<i8>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_i16() {
    let data = u16::MAX as i16;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<i16>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_i32() {
    let data = i32::MAX as i32;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<i32>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_i64() {
    let data = i64::MAX as i64;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<i64>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_u8() {
    let data = u8::MAX as u8;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &(data as u16));
    let result = from_keys_values::<u8>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_u16() {
    let data = u16::MAX as u16;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<u16>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_u32() {
    let data = u32::MAX as u32;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<u32>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_u64() {
    let data = (u32::MAX as u64 * 2_u64) as u64;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<u64>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_f32() {
    let data = f32::MAX as f32;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<f32>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_f64() {
    let data = f64::MAX as f64;

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result = from_keys_values::<f64>(&KeysValues { inner: vec![kv] }, Some(&subspace))
        .map_err(|err| println!("{:?}", err))
        .expect("");

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_char() {
    let data = 'a';

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &(data.to_string()));
    let result = from_keys_values::<char>(&KeysValues { inner: vec![kv] }, Some(&subspace));
    // char deserialization not supported
    assert!(result.is_err());
    if let Err(Error::CharDeserializationNotSupported { subspace }) = result {
        assert_eq!(subspace.bytes(), pack(&("_data")))
    } else {
        assert!(false)
    }
}

#[test]
fn test_deserialize_str() {
    let data = "a";

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result =
        from_keys_values::<String>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}

#[test]
fn test_deserialize_string() {
    let data = "a".to_string();

    let subspace = Subspace::all();
    let kv = create_key_value(create_data_field(None), &data);
    let result =
        from_keys_values::<String>(&KeysValues { inner: vec![kv] }, Some(&subspace)).unwrap();

    assert_eq!(result, data)
}
